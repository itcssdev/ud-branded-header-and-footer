document.addEventListener("mouseup",function(mEvent){
    var searchBox = document.getElementById('udHeaderSearch');
    var searchButton = document.getElementById('udHeaderSearchTrigger');
    if (searchBox!=mEvent.target && !searchBox.innerHTML.includes(mEvent.target.innerHTML) && searchButton!=mEvent.target) {
        searchBox.className = "udHeaderSearch hide";
        searchButton.className = "udHeaderSearchTrigger udHeaderOff";
    }
});

function toggleUDheaderMenuVisibility() {
    var headerMenu = document.getElementById('udHeaderMenu');
    var headerMenuButton = document.getElementById('udHeaderMenuTrigger');
    
    if (headerMenu.className == "udHeaderMenu hide") {
        headerMenu.className = "udHeaderMenu show";
        headerMenuButton.className = "udHeaderMenuTrigger udHeaderOn";
    }
    else {
        headerMenu.className = "udHeaderMenu hide";
        headerMenuButton.className = "udHeaderMenuTrigger udHeaderOff";
    }
    toggleUDheaderSearchVisibility();
};

function toggleUDheaderSearchVisibility() {
    var searchBox = document.getElementById('udHeaderSearch');
    var searchButton = document.getElementById('udHeaderSearchTrigger');
    
    if (searchBox.className == "udHeaderSearch hide") {
        searchBox.className = "udHeaderSearch show";
        searchButton.className = "udHeaderSearchTrigger udHeaderOn";
    }
    else {
        searchBox.className = "udHeaderSearch hide";
        searchButton.className = "udHeaderSearchTrigger udHeaderOff";
    }
};