Demos to preview the UD branded headers (search and search-free) can be found in the following files:

- demo.html
- demo-no-search.html

=======================

References to all required JS and CSS files can be found in the following file (to be placed in the head of your site):

- header-refs.html

=======================

Markup for the header, itself, can be found in the following files (to be placed as the first element following your opening body tag):

- header-markup.html (for sites including the UD CSE)
- header-markup-no-search.html (for sites not including the UD CSE)

=======================

Customizations:

- Any file paths may be adjusted to accommodate site structure, file locations and naming conventions.

- The interior header width may be altered based on site content width (noted in CSS file) for proper alignment.

- Any visual identity (dept name, sub title, etc.) should be placed at least 15px beneath the main site title. A mobile navigational trigger (e.g. hamburger menu) may be placed in the blue bar, aligned to the right.

=======================

Fonts:

- Both brand fonts, Greycliff and Vanguard, are made available to your site simply by implementing the header.

- To utilize any particular font/weight, call one or more of the following font-families on a desired element within your site CSS: ‘greycliff-bold’, ‘greycliff-light’ and/or ’vanguard-regular’.

	- Example: p { font-family: ‘greycliff-light’; }

- If additional font weights are required, please contact digitalteam@udel.edu