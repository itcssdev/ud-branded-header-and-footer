Demo to preview the UD branded footer can be found in the following file:

- demo.html

=======================

Reference to the required CSS can be found in the following file (to be placed in the head of your site):

- footer-refs.html

=======================


Markup for the footer, itself, can be found in the following file (to be placed as the last element within your body tag):

- footer-markup.html

=======================

Customizations:

Any file paths may be adjusted to accommodate site structure, file locations and naming conventions.